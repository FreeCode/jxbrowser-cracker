```stdshell
wget https://maven.teamdev.com/repository/products/com/teamdev/jxbrowser/jxbrowser-linux64/6.22/jxbrowser-linux64-6.22.jar
wget https://maven.teamdev.com/repository/products/com/teamdev/jxbrowser/jxbrowser-win64/6.22/jxbrowser-win64-6.22.jar
wget https://maven.teamdev.com/repository/products/com/teamdev/jxbrowser/jxbrowser-win32/6.22/jxbrowser-win32-6.22.jar
wget https://maven.teamdev.com/repository/products/com/teamdev/jxbrowser/jxbrowser-mac/6.22/jxbrowser-mac-6.22.jar
wget https://maven.teamdev.com/repository/products/com/teamdev/jxbrowser/jxbrowser/6.22/jxbrowser-6.22-javadoc.jar
wget https://maven.teamdev.com/repository/products/com/teamdev/jxbrowser/jxbrowser/6.22/jxbrowser-6.22.jar


cd D:\java\srcode\jxbrowser-cracker\lib
mvn install:install-file -Dpackaging=jar -DgeneratePom=true -DcreateChecksum=true -DgroupId=com.teamdev.jxbrowser -DartifactId=jxbrowser-win32 -Dversion=6.22 -Dfile=jxbrowser-win32-6.22.jar
mvn install:install-file -Dpackaging=jar -DgeneratePom=true -DcreateChecksum=true -DgroupId=com.teamdev.jxbrowser -DartifactId=jxbrowser-win64 -Dversion=6.22 -Dfile=jxbrowser-win64-6.22.jar
mvn install:install-file -Dpackaging=jar -DgeneratePom=true -DcreateChecksum=true -DgroupId=com.teamdev.jxbrowser -DartifactId=jxbrowser-mac -Dversion=6.22 -Dfile=jxbrowser-mac-6.22.jar
mvn install:install-file -Dpackaging=jar -DgeneratePom=true -DcreateChecksum=true -DgroupId=com.teamdev.jxbrowser -DartifactId=jxbrowser-linux64 -Dversion=6.22 -Dfile=jxbrowser-linux64-6.22.jar
mvn install:install-file -Dpackaging=jar -DgeneratePom=true -DcreateChecksum=true -DgroupId=com.teamdev.jxbrowser -DartifactId=jxbrowser -Dversion=6.22 -Dfile=jxbrowser-6.22.jar

```




